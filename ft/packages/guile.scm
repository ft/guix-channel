;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Frank Terbeck <ft@bewatermyfriend.org>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (ft packages guile)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texinfo))

;; Here's a utility for defining some packages that use plain Makefiles to
;; build Guile modules as well is (if available) documentation. The basic
;; operation is this:
;;
;; make CC=gcc                                  (in case of native builds)
;; make CC=gcc CFLAGS_ADD=-Tthat-arch-triple    (in case of cross-builds)
;; make doc                                     (in case documentation exists)
;; make test
;; make install PREFIX= DESTDIR=%OUTDIR% DOCDIR=/share/doc/PKGNAME-PKGVERSION
;;
;; This function is used to define the guile-e-series, guile-lfsr and
;; guile-srfi-151 packages.
(define* (ft/guile-pkg #:key
                       pkg-name pkg-version
                       pkg-url pkg-homepage
                       pkg-commit pkg-sha256
                       pkg-synopsis pkg-description pkg-licence
                       pkg-inputs pkg-native-inputs
                       build-doc?)
  (define (ftgl n s) (string-append "https://gitlab.com/ft/" n s))
  (let ((pkg-url (or pkg-url (ftgl pkg-name ".git")))
        (pkg-homepage (or pkg-homepage (ftgl pkg-name "")))
        (pkg-commit (or pkg-commit (string-append "v" pkg-version))))
    (package
      (name pkg-name)
      (version pkg-version)
      (source
       (origin
         (file-name (string-append pkg-name "-" pkg-commit "-checkout"))
         (method git-fetch)
         (uri (git-reference (url pkg-url) (commit pkg-commit)))
         (sha256 (base32 pkg-sha256))))
      (inputs pkg-inputs)
      (native-inputs pkg-native-inputs)
      (arguments `(#:phases
                   (modify-phases %standard-phases
                     (delete 'configure)
                     (delete 'install-license-files)
                     (delete 'strip)
                     (replace 'build
                       (lambda* (#:key target #:allow-other-keys)
                         (setenv "GUILE_AUTO_COMPILE" "0")
                         (let ((cmd (cons*
                                     "make" "CC=gcc"
                                     (if target
                                         (list (string-append "CFLAGS_ADD=-T"
                                                              target))
                                         '()))))
                           (apply invoke cmd)
                           (when ,build-doc? (invoke "make" "doc")))))
                     (replace 'install
                       (lambda* (#:key outputs #:allow-other-keys)
                         (let ((out (assoc-ref outputs "out")))
                           (invoke "make" "install"
                                   (string-append "DESTDIR=" out)
                                   (string-append "DOCDIR=" "/share/doc/"
                                                  ,pkg-name "-" ,pkg-version)
                                   "PREFIX=")))))
                   #:test-target "test"))     (build-system gnu-build-system)
      (synopsis pkg-synopsis)
      (description pkg-description)
      (home-page pkg-homepage)
      (license pkg-licence))))

(define guile-srfi-151-description
  "This SRFI proposes a coherent and comprehensive set of procedures for
performing bitwise logical operations on integers.")

(define-public guile-srfi-151
  (let ((name "guile-srfi-151")
        (version "0.1.0"))
    (ft/guile-pkg
     #:pkg-name name
     #:pkg-synopsis "SRFI-151: Bitwise Operations for GNU Guile"
     #:pkg-description guile-srfi-151-description
     #:pkg-version version
     #:pkg-homepage "https://gitlab.com/ft/srfi-151"
     #:pkg-url "https://gitlab.com/ft/srfi-151.git"
     #:pkg-sha256 "0wrv87a260sh8c49cw4jv25badrs650axkp0rf0d58d3f7fqsl2g"
     #:pkg-licence license:bsd-2
     #:pkg-inputs `(("guile" ,guile-3.0))
     #:pkg-native-inputs `(("guile-tap" ,guile-tap)))))

(define guile-e-series-description
  "This library consists of tools to work with E series of preferred
numbers. The E series is a system of preferred numbers (also called
preferred values) derived for use in electronic components.")

(define-public guile-e-series
  (let ((name "guile-e-series")
        (version "0.1.2"))
    (ft/guile-pkg
     #:pkg-name name
     #:pkg-synopsis "E-Series Tools for GNU Guile"
     #:pkg-description guile-e-series-description
     #:pkg-version version
     #:pkg-sha256 "12jpdxi912lvxw906g241zg3mhn54ywwa7lai6rpm9nd8gnbnbfv"
     #:pkg-licence license:lgpl3+
     #:pkg-inputs `(("guile" ,guile-3.0))
     #:pkg-native-inputs `(("guile-tap" ,guile-tap)))))

(define guile-lfsr-description
  "The (communication lfsr) module implements linear feedback shift
registers as infinite streams of bits. LFSRs are used, for
example, to generate chip sequences in CDMA systems, for
synchronising preambles or as pseudo random number generators.
The module implements LFSRs in both Fibonacci and Galois
configurations.")

(define-public guile-lfsr
  (let ((name "guile-lfsr")
        (version "0.1.2"))
    (ft/guile-pkg
     #:pkg-name name
     #:pkg-synopsis "Linear Feedback Shift Registers for GNU Guile"
     #:pkg-description guile-lfsr-description
     #:pkg-version version
     #:pkg-sha256 "083vvwhl26whinlgjg5yyxicvj29n1r36dkhacnm2px2qlnfjvhm"
     #:pkg-licence license:lgpl3+
     #:pkg-inputs `(("guile" ,guile-3.0)
                    ("guile-srfi-151" ,guile-srfi-151))
     #:pkg-native-inputs `(("guile-tap" ,guile-tap)))))
